function Palindrome(str) {
    return str === str.split('').reverse().join('');
}

function findVowels(str) {
    return str.match(/[aeiou]/gi).length;
}

function Alphabeta(arr) {
    for (let i = 0 ; i < 9; i++) {
        if (arr[i] % 3 === 0 && arr[i] % 5 === 0) {
            arr[i] = "alphabeta";
        } else
        if(arr[i] % 3 === 0) {
            arr[i] = "alpha";
        } else
        if (arr[i] % 5 === 0) {
            arr[i] = "beta";
        }
    }
    return arr;
}


module.exports = Alphabeta;
module.exports = Palindrome;
module.exports = findVowels;