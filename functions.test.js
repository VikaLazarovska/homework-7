const request = require("request");
const publicIp = require('public-ip');

describe("Palindrome function", () => {
    const Palindrome = require("./functions");
    test("Eye", () => {
        expect(Palindrome("eye")).toBeTruthy();
    });

    test("Rotator", () => {
        expect(Palindrome("rotator")).toBeTruthy();
    });

    test("Racecar", () => {
        expect(Palindrome("racecar")).toBeTruthy();
    });

    test("Civic", () => {
        expect(Palindrome("civic")).toBeTruthy();
    });

    test("level", () => {
        expect(Palindrome("level")).toBeTruthy();
    });
});


describe("findVowels function", () => {
    const findVowels = require("./functions");

    test("Car length", () => {
        expect(findVowels("car")).toBe(1);
    });

    test("potato length", () => {
        expect(findVowels("potato")).toBe(3);
    });

    test("I likes potatoes", () => {
        expect(findVowels("I likes potatoes")).toBe(7);
    });

    test("table", () => {
        expect(findVowels("table")).toBe(2);
    });

    test("Waffles", () => {
        expect(findVowels("waffles")).toBe(2);
    });
});

describe.only("Alphabeta function", () => {
    const Alphabeta = require("./functions");

    test("Test 1 ", () => {
        let arr = [5, 2, 4];
        let result = ["beta", 2, 4 ];
        expect(Alphabeta(arr)).toEqual(result);
    });

    test("Test2", () => {
        let arr = [1, 15, 8];
        let result = [1, "alphabeta", 8];
        expect(Alphabeta(arr)).toEqual(result);
    });
    test("Test 3", () => {
        let arr = [20, 3, 7];
        let result = ["beta", "alpha", 7 ];
        expect(Alphabeta(arr)).toEqual(result);
    });
    test("Test 4", () => {
        let arr = [1, 8, 6];
        let result = [1, 8, "alpha"];
        expect(Alphabeta(arr)).toEqual(result);
    });
    test("Test 5", () => {
        let arr = [45, 89, 11];
        let result = ["alphabeta", 89, 11];
        expect(Alphabeta(arr)).toEqual(result);
    });
});

describe("Ip test", () => {
    test("Ip", async () => {
        request("https://httpbin.org/get", async function(
            error,
            response
        ) {
            let ipv4 = await publicIp.v4({ onlyHttps: true });
            let obj = JSON.parse(response.body);
            expect(response.origin).toBe(ipv4)
            console.log("obj", obj[0].origin);
        });
    });
});